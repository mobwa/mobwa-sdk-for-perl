=begin comment

Mobwa Payments Hub

Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 

The version of the OpenAPI document: 1.0.0

Generated by: https://openapi-generator.tech

=end comment

=cut

#
# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# Do not edit the class manually.
# Ref: https://openapi-generator.tech
#
package Mobwa::UsersApi;

require 5.6.0;
use strict;
use warnings;
use utf8; 
use Exporter;
use Carp qw( croak );
use Log::Any qw($log);

use Mobwa::ApiClient;

use base "Class::Data::Inheritable";

__PACKAGE__->mk_classdata('method_documentation' => {});

sub new {
    my $class = shift;
    my $api_client;

    if ($_[0] && ref $_[0] && ref $_[0] eq 'Mobwa::ApiClient' ) {
        $api_client = $_[0];
    } else {
        $api_client = Mobwa::ApiClient->new(@_);
    }

    bless { api_client => $api_client }, $class;

}


#
# create_user
#
# Create a user
# 
# @param CreateUserRequest $create_user_request  (optional)
{
    my $params = {
    'create_user_request' => {
        data_type => 'CreateUserRequest',
        description => '',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'create_user' } = { 
        summary => 'Create a user',
        params => $params,
        returns => 'ARRAY[Transfer]',
        };
}
# @return ARRAY[Transfer]
#
sub create_user {
    my ($self, %args) = @_;

    # parse inputs
    my $_resource_path = '/users';

    my $_method = 'POST';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json');

    my $_body_data;
    # body params
    if ( exists $args{'create_user_request'}) {
        $_body_data = $args{'create_user_request'};
    }

    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Transfer]', $response);
    return $_response_object;
}

#
# get_user
#
# Get user information
# 
# @param string $user_id The id of the user to operate on (required)
{
    my $params = {
    'user_id' => {
        data_type => 'string',
        description => 'The id of the user to operate on',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_user' } = { 
        summary => 'Get user information',
        params => $params,
        returns => 'Account',
        };
}
# @return Account
#
sub get_user {
    my ($self, %args) = @_;

    # verify the required parameter 'user_id' is set
    unless (exists $args{'user_id'}) {
      croak("Missing the required parameter 'user_id' when calling get_user");
    }

    # parse inputs
    my $_resource_path = '/users/{userId}';

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type();

    # path params
    if ( exists $args{'user_id'}) {
        my $_base_variable = "{" . "userId" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'user_id'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Account', $response);
    return $_response_object;
}

#
# list_users
#
# List all users
# 
{
    my $params = {
    };
    __PACKAGE__->method_documentation->{ 'list_users' } = { 
        summary => 'List all users',
        params => $params,
        returns => 'ARRAY[User]',
        };
}
# @return ARRAY[User]
#
sub list_users {
    my ($self, %args) = @_;

    # parse inputs
    my $_resource_path = '/users';

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type();

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[User]', $response);
    return $_response_object;
}

#
# update_user
#
# Update user information
# 
# @param string $user_id The id of the user to operate on (required)
# @param UpdateAccountRequest $update_account_request  (optional)
{
    my $params = {
    'user_id' => {
        data_type => 'string',
        description => 'The id of the user to operate on',
        required => '1',
    },
    'update_account_request' => {
        data_type => 'UpdateAccountRequest',
        description => '',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'update_user' } = { 
        summary => 'Update user information',
        params => $params,
        returns => 'Account',
        };
}
# @return Account
#
sub update_user {
    my ($self, %args) = @_;

    # verify the required parameter 'user_id' is set
    unless (exists $args{'user_id'}) {
      croak("Missing the required parameter 'user_id' when calling update_user");
    }

    # parse inputs
    my $_resource_path = '/users/{userId}';

    my $_method = 'PUT';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json');

    # path params
    if ( exists $args{'user_id'}) {
        my $_base_variable = "{" . "userId" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'user_id'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # body params
    if ( exists $args{'update_account_request'}) {
        $_body_data = $args{'update_account_request'};
    }

    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Account', $response);
    return $_response_object;
}

1;
