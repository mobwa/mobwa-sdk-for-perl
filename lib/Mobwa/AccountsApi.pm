=begin comment

Mobwa Payments Hub

Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 

The version of the OpenAPI document: 1.0.0

Generated by: https://openapi-generator.tech

=end comment

=cut

#
# NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
# Do not edit the class manually.
# Ref: https://openapi-generator.tech
#
package Mobwa::AccountsApi;

require 5.6.0;
use strict;
use warnings;
use utf8; 
use Exporter;
use Carp qw( croak );
use Log::Any qw($log);

use Mobwa::ApiClient;

use base "Class::Data::Inheritable";

__PACKAGE__->mk_classdata('method_documentation' => {});

sub new {
    my $class = shift;
    my $api_client;

    if ($_[0] && ref $_[0] && ref $_[0] eq 'Mobwa::ApiClient' ) {
        $api_client = $_[0];
    } else {
        $api_client = Mobwa::ApiClient->new(@_);
    }

    bless { api_client => $api_client }, $class;

}


#
# create_account
#
# Create an account
# 
# @param CreateAccountRequest $create_account_request  (optional)
{
    my $params = {
    'create_account_request' => {
        data_type => 'CreateAccountRequest',
        description => '',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'create_account' } = { 
        summary => 'Create an account',
        params => $params,
        returns => 'ARRAY[Transfer]',
        };
}
# @return ARRAY[Transfer]
#
sub create_account {
    my ($self, %args) = @_;

    # parse inputs
    my $_resource_path = '/accounts';

    my $_method = 'POST';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json');

    my $_body_data;
    # body params
    if ( exists $args{'create_account_request'}) {
        $_body_data = $args{'create_account_request'};
    }

    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Transfer]', $response);
    return $_response_object;
}

#
# get_account
#
# Get account information
# 
# @param string $account_id The id of the account (required)
{
    my $params = {
    'account_id' => {
        data_type => 'string',
        description => 'The id of the account',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_account' } = { 
        summary => 'Get account information',
        params => $params,
        returns => 'Account',
        };
}
# @return Account
#
sub get_account {
    my ($self, %args) = @_;

    # verify the required parameter 'account_id' is set
    unless (exists $args{'account_id'}) {
      croak("Missing the required parameter 'account_id' when calling get_account");
    }

    # parse inputs
    my $_resource_path = '/accounts/{accountId}';

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type();

    # path params
    if ( exists $args{'account_id'}) {
        my $_base_variable = "{" . "accountId" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'account_id'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Account', $response);
    return $_response_object;
}

#
# list_account
#
# List all accounts
# 
{
    my $params = {
    };
    __PACKAGE__->method_documentation->{ 'list_account' } = { 
        summary => 'List all accounts',
        params => $params,
        returns => 'ARRAY[Account]',
        };
}
# @return ARRAY[Account]
#
sub list_account {
    my ($self, %args) = @_;

    # parse inputs
    my $_resource_path = '/accounts';

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type();

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Account]', $response);
    return $_response_object;
}

#
# recharge_account
#
# Recharge the account
# 
# @param string $account_id The id of the account (required)
# @param RechargeAccountRequest $recharge_account_request  (optional)
{
    my $params = {
    'account_id' => {
        data_type => 'string',
        description => 'The id of the account',
        required => '1',
    },
    'recharge_account_request' => {
        data_type => 'RechargeAccountRequest',
        description => '',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'recharge_account' } = { 
        summary => 'Recharge the account',
        params => $params,
        returns => 'Account',
        };
}
# @return Account
#
sub recharge_account {
    my ($self, %args) = @_;

    # verify the required parameter 'account_id' is set
    unless (exists $args{'account_id'}) {
      croak("Missing the required parameter 'account_id' when calling recharge_account");
    }

    # parse inputs
    my $_resource_path = '/accounts/{accountId}/recharge';

    my $_method = 'POST';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json');

    # path params
    if ( exists $args{'account_id'}) {
        my $_base_variable = "{" . "accountId" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'account_id'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # body params
    if ( exists $args{'recharge_account_request'}) {
        $_body_data = $args{'recharge_account_request'};
    }

    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Account', $response);
    return $_response_object;
}

#
# update_account
#
# Update account information
# 
# @param string $account_id The id of the account (required)
# @param UpdateAccountRequest $update_account_request  (optional)
{
    my $params = {
    'account_id' => {
        data_type => 'string',
        description => 'The id of the account',
        required => '1',
    },
    'update_account_request' => {
        data_type => 'UpdateAccountRequest',
        description => '',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'update_account' } = { 
        summary => 'Update account information',
        params => $params,
        returns => 'Account',
        };
}
# @return Account
#
sub update_account {
    my ($self, %args) = @_;

    # verify the required parameter 'account_id' is set
    unless (exists $args{'account_id'}) {
      croak("Missing the required parameter 'account_id' when calling update_account");
    }

    # parse inputs
    my $_resource_path = '/accounts/{accountId}';

    my $_method = 'PUT';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json');

    # path params
    if ( exists $args{'account_id'}) {
        my $_base_variable = "{" . "accountId" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'account_id'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # body params
    if ( exists $args{'update_account_request'}) {
        $_body_data = $args{'update_account_request'};
    }

    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Account', $response);
    return $_response_object;
}

#
# withdraw_money
#
# Withdraw money from the account
# 
# @param string $account_id The id of the account (required)
# @param RechargeAccountRequest $body  (optional)
{
    my $params = {
    'account_id' => {
        data_type => 'string',
        description => 'The id of the account',
        required => '1',
    },
    'body' => {
        data_type => 'RechargeAccountRequest',
        description => '',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'withdraw_money' } = { 
        summary => 'Withdraw money from the account',
        params => $params,
        returns => 'Account',
        };
}
# @return Account
#
sub withdraw_money {
    my ($self, %args) = @_;

    # verify the required parameter 'account_id' is set
    unless (exists $args{'account_id'}) {
      croak("Missing the required parameter 'account_id' when calling withdraw_money");
    }

    # parse inputs
    my $_resource_path = '/accounts/{accountId}/withdraw';

    my $_method = 'POST';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json');

    # path params
    if ( exists $args{'account_id'}) {
        my $_base_variable = "{" . "accountId" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'account_id'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # body params
    if ( exists $args{'body'}) {
        $_body_data = $args{'body'};
    }

    # authentication setting, if any
    my $auth_settings = [qw(basicAuth )];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Account', $response);
    return $_response_object;
}

1;
