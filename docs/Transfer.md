# Mobwa::Object::Transfer

## Load the model package
```perl
use Mobwa::Object::Transfer;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**currency** | **string** |  | [optional] 
**created_at** | **string** |  | [optional] 
**updated_at** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**auto_complete** | **boolean** |  | [optional] 
**source** | [**TransferSource**](TransferSource.md) |  | [optional] 
**destination** | [**TransferSource**](TransferSource.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


