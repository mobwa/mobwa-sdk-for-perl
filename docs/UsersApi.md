# Mobwa::UsersApi

## Load the API package
```perl
use Mobwa::Object::UsersApi;
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_user**](UsersApi.md#create_user) | **POST** /users | Create a user
[**get_user**](UsersApi.md#get_user) | **GET** /users/{userId} | Get user information
[**list_users**](UsersApi.md#list_users) | **GET** /users | List all users
[**update_user**](UsersApi.md#update_user) | **PUT** /users/{userId} | Update user information


# **create_user**
> ARRAY[Transfer] create_user(create_user_request => $create_user_request)

Create a user

### Example 
```perl
use Data::Dumper;
use Mobwa::UsersApi;
my $api_instance = Mobwa::UsersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $create_user_request = Mobwa::Object::CreateUserRequest->new(); # CreateUserRequest | 

eval { 
    my $result = $api_instance->create_user(create_user_request => $create_user_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling UsersApi->create_user: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_user_request** | [**CreateUserRequest**](CreateUserRequest.md)|  | [optional] 

### Return type

[**ARRAY[Transfer]**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user**
> Account get_user(user_id => $user_id)

Get user information

### Example 
```perl
use Data::Dumper;
use Mobwa::UsersApi;
my $api_instance = Mobwa::UsersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $user_id = "user_id_example"; # string | The id of the user to operate on

eval { 
    my $result = $api_instance->get_user(user_id => $user_id);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling UsersApi->get_user: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **string**| The id of the user to operate on | 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_users**
> ARRAY[User] list_users()

List all users

### Example 
```perl
use Data::Dumper;
use Mobwa::UsersApi;
my $api_instance = Mobwa::UsersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);


eval { 
    my $result = $api_instance->list_users();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling UsersApi->list_users: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ARRAY[User]**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user**
> Account update_user(user_id => $user_id, update_account_request => $update_account_request)

Update user information

### Example 
```perl
use Data::Dumper;
use Mobwa::UsersApi;
my $api_instance = Mobwa::UsersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $user_id = "user_id_example"; # string | The id of the user to operate on
my $update_account_request = Mobwa::Object::UpdateAccountRequest->new(); # UpdateAccountRequest | 

eval { 
    my $result = $api_instance->update_user(user_id => $user_id, update_account_request => $update_account_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling UsersApi->update_user: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **string**| The id of the user to operate on | 
 **update_account_request** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

