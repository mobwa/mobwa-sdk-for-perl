# Mobwa::Object::CreateUserRequest

## Load the model package
```perl
use Mobwa::Object::CreateUserRequest;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**password** | **string** |  | [optional] 
**role** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


