# Mobwa::Object::Account

## Load the model package
```perl
use Mobwa::Object::Account;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**created_at** | **string** |  | [optional] 
**currency** | **string** |  | [optional] 
**updated_at** | **string** |  | [optional] 
**balance** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


