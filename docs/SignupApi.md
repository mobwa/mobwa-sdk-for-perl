# Mobwa::SignupApi

## Load the API package
```perl
use Mobwa::Object::SignupApi;
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**signup**](SignupApi.md#signup) | **POST** /signup | Signup


# **signup**
> SignUpResponse signup(sign_up_request => $sign_up_request)

Signup

### Example 
```perl
use Data::Dumper;
use Mobwa::SignupApi;
my $api_instance = Mobwa::SignupApi->new(
);

my $sign_up_request = Mobwa::Object::SignUpRequest->new(); # SignUpRequest | 

eval { 
    my $result = $api_instance->signup(sign_up_request => $sign_up_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling SignupApi->signup: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sign_up_request** | [**SignUpRequest**](SignUpRequest.md)|  | [optional] 

### Return type

[**SignUpResponse**](SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

