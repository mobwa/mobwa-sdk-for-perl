# Mobwa::Object::SignUpResponse

## Load the model package
```perl
use Mobwa::Object::SignUpResponse;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**role** | **string** |  | [optional] 
**accounts** | [**ARRAY[Account]**](Account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


