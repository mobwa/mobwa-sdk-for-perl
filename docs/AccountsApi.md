# Mobwa::AccountsApi

## Load the API package
```perl
use Mobwa::Object::AccountsApi;
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_account**](AccountsApi.md#create_account) | **POST** /accounts | Create an account
[**get_account**](AccountsApi.md#get_account) | **GET** /accounts/{accountId} | Get account information
[**list_account**](AccountsApi.md#list_account) | **GET** /accounts | List all accounts
[**recharge_account**](AccountsApi.md#recharge_account) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**update_account**](AccountsApi.md#update_account) | **PUT** /accounts/{accountId} | Update account information
[**withdraw_money**](AccountsApi.md#withdraw_money) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account


# **create_account**
> ARRAY[Transfer] create_account(create_account_request => $create_account_request)

Create an account

### Example 
```perl
use Data::Dumper;
use Mobwa::AccountsApi;
my $api_instance = Mobwa::AccountsApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $create_account_request = Mobwa::Object::CreateAccountRequest->new(); # CreateAccountRequest | 

eval { 
    my $result = $api_instance->create_account(create_account_request => $create_account_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountsApi->create_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_account_request** | [**CreateAccountRequest**](CreateAccountRequest.md)|  | [optional] 

### Return type

[**ARRAY[Transfer]**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account**
> Account get_account(account_id => $account_id)

Get account information

### Example 
```perl
use Data::Dumper;
use Mobwa::AccountsApi;
my $api_instance = Mobwa::AccountsApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account

eval { 
    my $result = $api_instance->get_account(account_id => $account_id);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountsApi->get_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_account**
> ARRAY[Account] list_account()

List all accounts

### Example 
```perl
use Data::Dumper;
use Mobwa::AccountsApi;
my $api_instance = Mobwa::AccountsApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);


eval { 
    my $result = $api_instance->list_account();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountsApi->list_account: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ARRAY[Account]**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recharge_account**
> Account recharge_account(account_id => $account_id, recharge_account_request => $recharge_account_request)

Recharge the account

### Example 
```perl
use Data::Dumper;
use Mobwa::AccountsApi;
my $api_instance = Mobwa::AccountsApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $recharge_account_request = Mobwa::Object::RechargeAccountRequest->new(); # RechargeAccountRequest | 

eval { 
    my $result = $api_instance->recharge_account(account_id => $account_id, recharge_account_request => $recharge_account_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountsApi->recharge_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **recharge_account_request** | [**RechargeAccountRequest**](RechargeAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_account**
> Account update_account(account_id => $account_id, update_account_request => $update_account_request)

Update account information

### Example 
```perl
use Data::Dumper;
use Mobwa::AccountsApi;
my $api_instance = Mobwa::AccountsApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $update_account_request = Mobwa::Object::UpdateAccountRequest->new(); # UpdateAccountRequest | 

eval { 
    my $result = $api_instance->update_account(account_id => $account_id, update_account_request => $update_account_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountsApi->update_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **update_account_request** | [**UpdateAccountRequest**](UpdateAccountRequest.md)|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **withdraw_money**
> Account withdraw_money(account_id => $account_id, body => $body)

Withdraw money from the account

### Example 
```perl
use Data::Dumper;
use Mobwa::AccountsApi;
my $api_instance = Mobwa::AccountsApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $body = Mobwa::Object::RechargeAccountRequest->new(); # RechargeAccountRequest | 

eval { 
    my $result = $api_instance->withdraw_money(account_id => $account_id, body => $body);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountsApi->withdraw_money: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **body** | **RechargeAccountRequest**|  | [optional] 

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

