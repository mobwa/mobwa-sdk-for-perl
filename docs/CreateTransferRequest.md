# Mobwa::Object::CreateTransferRequest

## Load the model package
```perl
use Mobwa::Object::CreateTransferRequest;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**currency** | **string** |  | [optional] 
**auto_complete** | **boolean** |  | [optional] 
**destination** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


