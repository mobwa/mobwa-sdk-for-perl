# Mobwa::Object::RechargeAccountRequest

## Load the model package
```perl
use Mobwa::Object::RechargeAccountRequest;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amout** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


