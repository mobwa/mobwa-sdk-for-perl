# Mobwa::Object::UpdateTransferRequest

## Load the model package
```perl
use Mobwa::Object::UpdateTransferRequest;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


