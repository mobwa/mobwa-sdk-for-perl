# Mobwa::TransfersApi

## Load the API package
```perl
use Mobwa::Object::TransfersApi;
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**complete_transfer**](TransfersApi.md#complete_transfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**create_transfer**](TransfersApi.md#create_transfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**delete_transfer**](TransfersApi.md#delete_transfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**get_transfer**](TransfersApi.md#get_transfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**list_transfers**](TransfersApi.md#list_transfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**update_transfer**](TransfersApi.md#update_transfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information


# **complete_transfer**
> Transfer complete_transfer(account_id => $account_id, transfer_id => $transfer_id)

Complete a transfer

### Example 
```perl
use Data::Dumper;
use Mobwa::TransfersApi;
my $api_instance = Mobwa::TransfersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $transfer_id = "transfer_id_example"; # string | The id of the transfer to operate on

eval { 
    my $result = $api_instance->complete_transfer(account_id => $account_id, transfer_id => $transfer_id);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransfersApi->complete_transfer: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **transfer_id** | **string**| The id of the transfer to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_transfer**
> Transfer create_transfer(account_id => $account_id, create_transfer_request => $create_transfer_request)

Create a transfer

### Example 
```perl
use Data::Dumper;
use Mobwa::TransfersApi;
my $api_instance = Mobwa::TransfersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $create_transfer_request = Mobwa::Object::CreateTransferRequest->new(); # CreateTransferRequest | 

eval { 
    my $result = $api_instance->create_transfer(account_id => $account_id, create_transfer_request => $create_transfer_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransfersApi->create_transfer: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **create_transfer_request** | [**CreateTransferRequest**](CreateTransferRequest.md)|  | [optional] 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_transfer**
> Transfer delete_transfer(account_id => $account_id, transfer_id => $transfer_id)

Delete a transfer

### Example 
```perl
use Data::Dumper;
use Mobwa::TransfersApi;
my $api_instance = Mobwa::TransfersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $transfer_id = "transfer_id_example"; # string | The id of the transfers to operate on

eval { 
    my $result = $api_instance->delete_transfer(account_id => $account_id, transfer_id => $transfer_id);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransfersApi->delete_transfer: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **transfer_id** | **string**| The id of the transfers to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transfer**
> Transfer get_transfer(account_id => $account_id, transfer_id => $transfer_id)

Retrieve information for a specific transfer

### Example 
```perl
use Data::Dumper;
use Mobwa::TransfersApi;
my $api_instance = Mobwa::TransfersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $transfer_id = "transfer_id_example"; # string | The id of the transfers to operate on

eval { 
    my $result = $api_instance->get_transfer(account_id => $account_id, transfer_id => $transfer_id);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransfersApi->get_transfer: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **transfer_id** | **string**| The id of the transfers to operate on | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_transfers**
> ARRAY[Transfer] list_transfers(account_id => $account_id)

List all transfers related to the account

### Example 
```perl
use Data::Dumper;
use Mobwa::TransfersApi;
my $api_instance = Mobwa::TransfersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account

eval { 
    my $result = $api_instance->list_transfers(account_id => $account_id);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransfersApi->list_transfers: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 

### Return type

[**ARRAY[Transfer]**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_transfer**
> Transfer update_transfer(account_id => $account_id, transfer_id => $transfer_id, update_transfer_request => $update_transfer_request)

Change transfer information

### Example 
```perl
use Data::Dumper;
use Mobwa::TransfersApi;
my $api_instance = Mobwa::TransfersApi->new(

    # Configure HTTP basic authorization: basicAuth
    username => 'YOUR_USERNAME',
    password => 'YOUR_PASSWORD',
    
);

my $account_id = "account_id_example"; # string | The id of the account
my $transfer_id = "transfer_id_example"; # string | The id of the transfers to operate on
my $update_transfer_request = Mobwa::Object::UpdateTransferRequest->new(); # UpdateTransferRequest | 

eval { 
    my $result = $api_instance->update_transfer(account_id => $account_id, transfer_id => $transfer_id, update_transfer_request => $update_transfer_request);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransfersApi->update_transfer: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **string**| The id of the account | 
 **transfer_id** | **string**| The id of the transfers to operate on | 
 **update_transfer_request** | [**UpdateTransferRequest**](UpdateTransferRequest.md)|  | [optional] 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

